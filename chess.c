#include <stdlib.h>
#include <stdio.h>
#include "chess.h"
#include <string.h>

//metodo para invertir los colores de las figuras
//reemplaza cada caracter por su opuesto
char invColor(char c){
	switch( c ){
		case '_': return '=';
		case '=': return '_';
		case '.': return '@';
		case '@': return '.';
		case ' ': return ' ';
	}

	return '#';
} 

char** reverse(char** c){
	//inicializo los valores para el recorrido de las figuras
	int filas=58; //numero de filas de cada figura
	int columnas=58; //numero de columnas de cada figura
	int i,j;
	//recorremos cada fila de la figura
	for(i=0;i<filas;i++){
		//guardo en aux el caracter(char) de la posicion i de la cadena(figura)
		char* aux =*(c+i);
		//declaro para aux2 el tamaño en memoria del numero de columnas
		char* aux2 = (char*) malloc(sizeof(char)*columnas);
		//recorremos cada columna de la figura
		for(j=0;j<columnas;j++){
			//usamos la funcion invColor para la posicion j de aux y lo almacenamos en la posicion j de aux2
			aux2[j] = invColor(*(aux+j));
		}
		//reemplazamos el resultado de la cadena en la fila i de la figura c
		c[i] = aux2;
	}
	return c;
}

char** flipV(char** c){
	//inicializo los valores para el recorrido de las figuras
        int filas=58; //numero de filas de cada figura
        int columnas=58; //numero de columnas de cada figura
        int i,j;
	//creamos un espacio auxiliar de tamaño filas+1
	char** aux=(char**)malloc(sizeof(char*)*(filas+1));

        //recorremos cada fila de la figura
        for(i=0;i<filas;i++){
	 	aux[i]=(char*)malloc(sizeof(char)*(columnas+1));
	}
	for(i=0;i<filas;i++){
		for(j=0;j<columnas;j++){
			aux[i][j]=c[i][columnas-j-1];
		}
		aux[i][columnas]=0;
	}
	aux[filas]=0;
}

char** flipH(char** c){
	 //inicializo los valores para el recorrido de las figuras
        int filas=58; //numero de filas de cada figura
        int columnas=58; //numero de columnas de cada figura
        int i,j;
        //creamos un espacio auxiliar de tamaño filas+1
        char** aux=(char**)malloc(sizeof(char*)*(filas+1));

        //recorremos cada fila de la figura
        for(i=0;i<filas;i++){
                aux[i]=(char*)malloc(sizeof(char)*(columnas+1));
        }
        for(i=0;i<filas;i++){
                for(j=0;j<columnas;j++){
                        aux[i][j]=c[filas-i-1][j];
                }
                aux[i][columnas]=0;
        }
        aux[filas]=0;

}

char** join(char** c,char** c2){
	int filas=58;
	int columnas=58;
	int i,j;

	int totalColumnas=columnas*2;

	char** aux=(char**)malloc(sizeof(char*)*(filas+1));

	for(i=0;i<filas;i++){
		aux[i]=(char*)malloc(sizeof(char)*(totalColumnas+1));
	}
	for(i=0;i<filas;i++){
		for(j=0;j<totalColumnas;j++){
			aux[i][j]=c[i][j];
			if(j>=columnas){
				aux[i][j]=c2[i][j-columnas];
			}
		}
		aux[i][totalColumnas]=0;
	}
	aux[filas]=0;

	return aux;
}

char** up(char** c1, char** c2){

	int filas=58;
        int columnas=58;
        int i,j;

	int totalFilas=filas*2;

        char** aux=(char**)malloc(sizeof(char*)*(totalFilas+1));
	
	for(i=0;i<totalFilas;i++){
		for(j=0;j<columnas;j++){
			if(i<filas){
				aux[i][j]=c1[i][j];
			}else{
				aux[i][j]=c2[i-filas][j];
			}
		}
		aux[i][columnas]=0;
	}
	
	aux[totalFilas]=0;
	return aux;

}

char** rotateL(char** c){

	int filas=58;
        int columnas=58;
        int i,j;

	char** aux=(char**)malloc(sizeof(char*)*(filas+1));

	for(i=0;i<filas;i++){
                aux[i]=(char*)malloc(sizeof(char)*(totalColumnas+1));
        }
	for(i=0;i<filas;i++){
		for(j=0;j<columnas;j++){
			aux[i][j]=c[j][filas-i-1];
		}
		aux[i][columnas]=0;
	}
	aux[filas]=0;
	
	return aux;

}

char** rotateR(char** c){
	int filas=58;
        int columnas=58;
        int i,j;

	char** aux=(char**)malloc(sizeof(char*)*(filas+1));
	for(i=0;i<filas;i++){
		aux[i]=(char*)malloc(sizeof(char)*(columnas+1));
	}
	for(i=0;i<filas;i++){
		for(j=0;jcolumnas;j++){
			aux[i][j]=aux[columnas-j-1][i];
		}
		aux[i][columnas]=0;
	}
	aux[i]=0;

	return aux;
}

char** superImpose(char** c1, char** c2){
	int filas=58;
	int columnas=58;
	int i,j;

	char** aux=(char**)malloc(sizeof(char*)*(filas+1));

	for(i=0;i<filas;i++){
		aux[i]=(char*)malloc(sizeof(char)*(columnas+1));
	}
	for(i=0;i<filas;i++){
		for(j=0;j<columnas;j++){
			aux[i][j]=c[i][j];
		}
		aux[i][columnas]=0;
	}
	for(i=0;i<filas;i++){
		for(j=0;j<columnas;j++){
			if(aux[i][j]==' '){
				aux[i][j]=c2[i][j];
			}
		}
		aux[i][columnas]=0;
	}	
	aux[filas]=0;

	return aux;
}

char** repeatH(char** c; int n){
	int filas=58;
	int columnas=58;
	int i,j;
	
	int totalColumnas=columnas*n;
	
	char** aux=(char**)malloc(sizeof(char*)*(filas+1));

	for(i=0;i<filas;i++){
		aux[i]=(char*)malloc(sizeof(char)*(totalColumnas+1));
	}
	for(i=0;i<filas;i++){
		for(j=0;j<totalColumnas;j++){
			aux[i][j]=c[i][j%columnas];
		}
		aux[i][totalColumnas]=0;
	}
	aux[filas]=0;

	return aux;
}

char** repeatV(char** c;int n){
	
	int filas=58;
        int columnas=58;
        int i,j;

        int totalFilas=filas*n;

        char** aux=(char**)malloc(sizeof(char*)*(totalFilas+1));

        for(i=0;i<totalFilas;i++){
                aux[i]=(char*)malloc(sizeof(char)*(columnas+1));
        }
        for(i=0;i<totalFilas;i++){
                for(j=0;j<columnas;j++){
                        aux[i][j]=c[i%filas][j];
                }
                aux[i][columnas]=0;
        }
        aux[totalFilas]=0;

        return aux;

}
